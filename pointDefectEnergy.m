plotfunction [U, potentialEnergyTotal] = pointDefectEnergy(r, atomNumber, strength, alpha)
%Calculates the potential energy between atoms due to a pointDefect given by f = strength/r_ij^alpha
[N, ~] = size(r)
j = atomNumber
U = zeros(N)
for i = 1:N
    if i ~= j
       if alpha ~= 1
         dr = sqrt((r(i,1)-r(j,1))^2+ (r(i,2)-r(j,2))^2);
         U(i,j) = strength/dr^(alpha-1);
         U(j,i) = U(i,j);
       else
         dr = sqrt((r(i,1)-r(j,1))^2+ (r(i,2)-r(j,2))^2);
         U(i,j) = strength*log(r);
         U(j,i) = U(i,j);           
    end    
potentialEnergyTotal = sum(U);


end

