 function [gR, R] = GraphResistance(L)
%GraphResistance calculates the effective graph resistance of a graph given
%the graph Laplacian, L.
R = 0 * L;
[n, ~] = size(L);
Q = inv(L);
gR = 0; %graph resistance
for i = 1:n
    ei = zeros(n,1);
    ei(i) = 1;
    for j = i+1:n
        
        ej = zeros(n,1);
        ej(j) = 1;
        R(i,j) = transpose(ei - ej)*Q*(ei-ej); %effective resistance between nodes
        R(j,i) = R(i,j); %effective resistance is symmetric (in case I need the resistance matrix at some later time
        gR = gR + R(i,j);
    end
end

end

