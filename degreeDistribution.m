function [kDev, Nuk] = degreeDistribution(r, epsilon, varargin)
%Plots degree distribution of based on r and epsilon
%Returns Nu_k, kdev cells where cell{1} is the total force cell{2} is the
%energy weight cell{3} is potential energy weight and cell{4} is the
%attractive force and cell{5} is the repulsive force
[Na, dim] = size(r);
ro = 2^(1/6);
Nl = Na^(1/dim);
if nargin>2
    v = varargin{1};
else
    v = initv0(ro, Nl);
end
[forceMatrix, f, fa, fr] = forcecalc(r, epsilon, Nl*ro, Nl*ro, ro, Nl);
fad = sum(sum(fa,3));
frd = sum(sum(fr,3));
f2 = f.*f;
fmag = (f2(:,1) + f2(:,2)).^(1/2);
[uij, u] = potentialEnergy(r, epsilon, Nl*ro, Nl*ro);
v2 = v.*v;
vmag =  (v2(:,1) + v2(:,2)).^(1/2);
E = sum(uij) + transpose(vmag);
vertexWeights = {'Energy',E;
    'Potential Energy',sum(uij);
    'Force', transpose(fmag);
    'Attractive Force', fad;
    'Repulsive Force', frd;
}
[l, ~] = size(vertexWeights);
for i = 1:l
  kE = vertexWeights{i,2};
  k_max = 1.1*max(max(kE));
  if min(min(kE)) < 0
    k_min = 1.1*min(min(kE));
  else
    k_min = 0.9*min(min(kE));
  end
  dk = (k_max-k_min)/20;
  kDev{i} = k_min:dk:k_max;
  nu_k = hist(kE(:),kDev{i});
  Nuk{i} = nu_k/sum(nu_k);
  color = [1 0 1];
  figure
  hold on
  loglog(kDev{i},Nuk{i},'Marker','o','LineStyle','-',...
    'Color',[0 0 0],'MarkerFaceColor',color)
  ylabel('$\nu$(s)','Interpreter','latex','FontSize',30)
  xlabel('s','FontSize',30)
  title(vertexWeights{i,1})    
end




end

