function [uij, u] = potentialEnergyNB(r, epsilon)
% Calculates potential energy assuming perfect crystal boundaries. Returns a matrix [uij] of the individual reactions and u of the systems potential energy
% no boundary
[rij, ~] = distancematrix(r,r);
ua = attractiveEnergy(rij, epsilon);
ur = repulsiveEnergy(rij, epsilon);
[Na2, ~] = size(r);
Na = sqrt(Na2);
rinit = initr(2^(1/6), Na);
shift = 0*rinit;  % shift for perfect crystal boundaries
[~, dim] = size(shift);

uij = ua + ur;
u = sum(sum(uij));


end
