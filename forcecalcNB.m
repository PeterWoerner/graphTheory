function [force,f, fa, fr] = forcecalcNB(r, epsilon)
%UNTITLED5 Calculates forces for MD simulations, assumes no boundary forces
%   Detailed explanation goes here
[rij, dir] = distancematrix(r, r);
[fa, forcea(:,:,:)] = attractiveforce(rij, dir, epsilon);
[fr, forcer(:,:,:)] = repulsiveforce(rij,dir, epsilon);



force = fa + fr;
f= forcea + forcer;
f = sum(f,1);
%q = [f(1,:,1), f(1,:,2)];
%q = reshape(q,size(r));

% f = q;


end

