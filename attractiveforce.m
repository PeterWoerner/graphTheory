function [fij, force] = attractiveforce(rij, dir, epsilon)
%Attractive Froce  Calculates the attractive part of the force based on a distance matrix, direction matrix and the defect parameter epsilon
%Returns matrix of magnitudes and another 3 dimensional array of the individual components
[na,~, dim] = size(dir);
defect = ceil(na/2+sqrt(na)/2);
for i = 1:na
    for j= 1:na
        if rij(i,j) ~= 0
            if i ~= defect && j~=defect
                pf = 1;
            else
                pf = 1+epsilon;
            end
            fij(i,j) = 24*pf*rij(i,j)^-7;
            for k = 1:dim
                force(i,j,k) = fij(i,j)*dir(i,j,k);
            end
        else
            fij(i,j) = 0;
            for k = 1:dim
                force(i,j,k) = 0;
            end
        end
    end
end


end


