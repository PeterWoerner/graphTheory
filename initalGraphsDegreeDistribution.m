function [] = initalGraphsDegreeDistribution()
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
ro = 2^(1/6);
Na = 20;
N = Na^2;
r = initr(ro, Na);
[forceMatrix0, ~, fa0, fr0] = forcecalcNB(r, 0);
size(forceMatrix0);
%size(f0)
size(fa0);
size(fr0);
[uij0, ~] = potentialEnergyNB(r, 0);
Cases = {};
for i = 2:2
    epsilon = i/10;
    [forcematrix, ~, fa, fr] = forcecalcNB(r, epsilon);
    [uij, u] = potentialEnergyNB(r, epsilon);
    size(uij);
    dU = abs(uij0 - uij);
    dforce = abs(forceMatrix0 - forcematrix);
    dfa = abs(fa0 - fa);
    dfr = abs(fr0 - fr);
    degDist(dU, strcat('U \epsilon =',num2str(epsilon)))
    degDist(dforce, strcat('F \epsilon =',num2str(epsilon)))
    degDist(dfa, strcat('F_a \epsilon =',num2str(epsilon)))
    degDist(dfr, strcat('F_r \epsilon =',num2str(epsilon)))
    figure
    weightedGraphPlot(dU, r, sum(dU)*0);
    dUu  = dU*0;
    uu = dUu;
    fau = uu;
    fru = uu;
    fu = uu;
    dFu  = dUu;
    dFau = dUu;
    dFru = dUu;
    for j = 1:N
        for k = 1:N
%             if dU(j,k) ~= 0
%                 dUu(j,k) = 1;
%             end
%             if dforce ~= 0
%                 dFu(j,k)= 1;
%             end
%             if dfa ~= 0
%                 dFau(j,k) = 1;
%             end
%             if dfr ~= 0
%                 dFru(j,k) = 1;
%             end
            if abs(uij(j,k)) > 1e-4
                uu(j,k) = 1;                
            end
            if abs(forcematrix(j,k)) > 1e-4
                fu(j,k) = 1;                
            end            
            if abs(fa(j,k)) > 1e-4
                fau(j,k) = 1;                
            end            
            if abs(fr(j,k)) > 1e-4
                fru(j,k) = 1;                
            end            
        end
    end
    figure
    weightedGraphPlot(uu, r, sum(uu*0));
    uc = clusteringCoefficients(uu);
    fc = clusteringCoefficients(fu);
    fac = clusteringCoefficients(fau);
    frc = clusteringCoefficients(fru);
    distPlot(uc)% 'Clustering U \epsilon =',num2str(epsilon))
    distPlot(fc)%, 'Clustering F \epsilon =',num2str(epsilon))
    distPlot(fac)%, 'Clustering F_a \epsilon =',num2str(epsilon))
    distPlot(frc)%, 'Clustering F_r \epsilon =',num2str(epsilon))
    
%     dUc = clusteringCoefficients(dUu);
%     dfc = clusteringCoefficients(dFu);
%     dfac = clusteringCoefficients(dFau);
%     dfrc = clusteringCoefficients(dFru);
%     distPlot(dUc, 'Clustering U \epsilon =',num2str(epsilon))
%     distPlot(dfc, 'Clustering F \epsilon =',num2str(epsilon))
%     distPlot(dfac, 'Clustering F_a \epsilon =',num2str(epsilon))
%     distPlot(dfrc, 'Clustering F_r \epsilon =',num2str(epsilon))
end

end

function [] = distPlot(kE, varargin)

kmax = 1.1*max(max(kE))+1e-4;
if min(min(kE)) < 0
  kmin = 1.1*min(min(kE));
else
  kmin = 0.9*min(min(kE))-1e-4;
end
dk = (kmax-kmin)/20;
kDev = kmin:dk:kmax;
nuk = hist(kE(:), kDev);
Nuk = nuk/sum(nuk);
color = [1 0 1];
figure
hold on
loglog(kDev,Nuk,'Marker','o','LineStyle','-',...
  'Color',[0 0 0],'MarkerFaceColor',color)
ylabel('$\nu$(s)','Interpreter','latex','FontSize',30)
xlabel('s','FontSize',30)
if nargin > 1
    title(varargin{1}) 
end
end


function [] = degDist(A, varargin)
kE = sum(A);
kmax = 1.1*max(max(kE));
if min(min(kE)) < 0
  kmin = 1.1*min(min(kE));
else
  kmin = 0.9*min(min(kE));
end
dk = (kmax-kmin)/20;
kDev = kmin:dk:kmax;
nuk = hist(kE(:), kDev);
Nuk = nuk/sum(nuk);
color = [1 0 1];
figure
hold on
loglog(kDev,Nuk,'Marker','o','LineStyle','-',...
  'Color',[0 0 0],'MarkerFaceColor',color)
ylabel('$\nu$(s)','Interpreter','latex','FontSize',30)
xlabel('s','FontSize',30)
if nargin > 1
    title(varargin{1}) 
end

end