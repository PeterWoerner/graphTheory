function [lmax] = ralpha(varargin)
%Plots degree distribution of based on r and epsilon
%Returns Nu_k, kdev cells where cell{1} is the total force cell{2} is the
%energy weight cell{3} is potential energy weight and cell{4} is the
%attractive force and cell{5} is the repulsive force
close all
set(0,'DefaultLineLinewidth',2)
set(0,'DefaultAxesFontSize',20)
Na = 7;
alpha = 1; 
ro = 1;
willSave = true
if nargin > 1
    alpha = varargin{1}
    Na = varargin{2}
end
if nargin > 2
    willSave = varargin{3}
end
e = zeros(Na*Na);
r = initr(ro, Na);
[n,~] = size(e)
j = ceil(Na^2/2)
for i = 1:Na*Na
   
    if i ~= j
       dr = sqrt((r(i,1)-r(j,1))^2+ (r(i,2)-r(j,2))^2);
       e(i,j) = 1/dr^alpha;
       e(j,i) = e(i,j);
    end
     
end 
kE = sum(e);
kE = [kE(1:j-1), kE(j+1:Na*Na)];
i = 1;
%% Plot Graph
test = figure
weightedGraphPlot(e, r, kE*0);
if willSave
    filename = strcat('grapha_', num2str(alpha))
    saveas(test, filename, 'epsc')
    saveas(test, filename, 'fig')
    close all
end
%% Plot Degree Distribution
k_max = 1.1*max(max(kE));
if min(min(kE)) < 0
  k_min = 1.1*min(min(kE));
else
  k_min = 0.9*min(min(kE));
end
dk = (k_max-k_min)/30;
kDev{i} = k_min:dk:k_max;
nu_k = hist(kE(:),kDev{i});
Nuk{i} = nu_k/sum(nu_k);
color = [1 0 1];
figure
loglog(kDev{i},Nuk{i},'Marker','o','LineStyle','-',...
  'Color',[0 0 0],'MarkerFaceColor',color)
ylabel('$\nu$(s)','Interpreter','latex','FontSize',20)
xlabel('s','FontSize',30)
if willSave
    filename = strcat('degreeDista', num2str(alpha))
    saveas(gcf, filename, 'epsc')
    saveas(gcf, filename, 'fig')
    close all
end
figure
hist(Nuk{i})
if willSave
    filename = strcat('degreeDistHista', num2str(alpha))
    saveas(gcf, filename, 'epsc')
    saveas(gcf, filename, 'fig')
    close all
end
%% Plot Eigenvalue and Laplacian Spectra
eigG = eig(e);
eigL = eig(diag(sum(e))-e);

kE = eigG;
k_max = 1.1*max(max(kE));
if min(min(kE)) < 0
  k_min = 1.1*min(min(kE));
else
  k_min = 0.9*min(min(kE));
end
dk = (k_max-k_min)/30;
kDev{i} = k_min:dk:k_max;
nu_k = hist(kE(:),kDev{i});
Nuk{i} = nu_k/sum(nu_k);
color = [1 0 1];
figure
loglog(kDev{i},Nuk{i},'Marker','o','LineStyle','-',...
  'Color',[0 0 0],'MarkerFaceColor',color)
ylabel('$\nu$(s)','Interpreter','latex','FontSize',20)
xlabel('s','FontSize',30)
if willSave
    filename = strcat('eigAdjSpeca', num2str(alpha))
    saveas(gcf, filename, 'fig')
    saveas(gcf, filename, 'epsc')
    close all
end
figure
hist(eigG)
if willSave
    filename = strcat('eigAdjHista', num2str(alpha))
    saveas(gcf, filename, 'epsc')
    saveas(gcf, filename, 'fig')
    close all
end
figure
n = length(eigG)
hist(eigG(2:n-1))
if willSave
    filename = strcat('eigAdjHistMida', num2str(alpha))
    saveas(gcf, filename, 'fig')
    saveas(gcf, filename, 'epsc')
    close all
end

ke = eigL;
k_max = 1.1*max(max(kE));
if min(min(kE)) < 0
  k_min = 1.1*min(min(kE));
else
  k_min = 0.9*min(min(kE));
end
dk = (k_max-k_min)/30;
kDev{i} = k_min:dk:k_max;
nu_k = hist(kE(:),kDev{i});
Nuk{i} = nu_k/sum(nu_k);
color = [1 0 1];
figure
loglog(kDev{i},Nuk{i},'Marker','o','LineStyle','-',...
  'Color',[0 0 0],'MarkerFaceColor',color)
ylabel('$\nu$(s)','Interpreter','latex','FontSize',20)
xlabel('s','FontSize',30)
if willSave
    filename = strcat('eigLapSpeca', num2str(alpha))
    saveas(gcf, filename, 'fig')
    saveas(gcf, filename, 'epsc')
    close all
end
figure
hist(eigL)
if willSave
    filename = strcat('eigLapHista', num2str(alpha))
    saveas(gcf, filename, 'fig')
    saveas(gcf, filename, 'epsc')
    close all
end
[maxValue, row] = max(eigL)
n = length(eigL)

figure
hist(eigL(1:n-1))
if willSave
    filename = strcat('eigLapBota', num2str(alpha))
    saveas(gcf, filename, 'fig')
    saveas(gcf, filename, 'epsc')
    close all
end
lmax = eigL(n)
end

