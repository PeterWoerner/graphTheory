function [uij, u] = potentialEnergy(r, epsilon, lx, ly)
% Calculates potential energy assuming perfect crystal boundaries. Returns a matrix [uij] of the individual reactions and u of the systems potential energy
[rij, ~] = distancematrix(r,r);
ua = attractiveEnergy(rij, epsilon);
ur = repulsiveEnergy(rij, epsilon);
[Na2, ~] = size(r);
Na = sqrt(Na2);
rinit = initr(2^(1/6), Na);
shift = 0*rinit;  % shift for perfect crystal boundaries
[~, dim] = size(shift);

 for i = -1:1
     for j = -1:1
         if i == 0 & j == 0
             0;
         else      
             shift(:,1) = i*lx;
             shift(:,2) = j*ly;
             [rij, dir] = distancematrix(r, rinit+shift);
             ua(:,:, j+2+3*(i+1)) = attractiveEnergy(rij, epsilon);
             ur(:,:, j+2+3*(i+1)) = repulsiveEnergy(rij, epsilon);
         end
     end
 end

uij = sum(ua,3) + sum(ur,3);
u = sum(sum(uij));


end
