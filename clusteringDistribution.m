function [kDev, Nuk] = clusteringDistribution(r, epsilon)
%Plots clustering distribution of based on r and epsilon
%Two methods, first is the cut off method, second is the 
%Returns Nu_k, kdev cells where cell{1} is the total force cell{2} is the
%energy weight cell{3} is potential energy weight and cell{4} is the
%attractive force and cell{5} is the repulsive force
%This code is presumably working however, the results do not seem correct
%to me.  More validation is required.

[Na, dim] = size(r);
ro = 2^(1/6);
Nl = Na^(1/dim);
[forceMatrix, f, fa, fr] = forcecalc(r, epsilon, Nl*ro, Nl*ro, ro, Nl);
fa(:,:,5);
fad = sum(sum(fa,3));
frd = sum(sum(fr,3));
f2 = f.*f;
fmag = (f2(:,1) + f2(:,2)).^(1/2);
[uij, u] = potentialEnergy(r, epsilon, Nl*ro, Nl*ro);
uij;
vertexWeights = {
    'Potential Energy',uij;
    'Force', forceMatrix(:,:,5);
    'Attractive Force', fa(:,:,5);
    'Repulsive Force', fr(:,:,5);
};
vertexClusters = {};
[l, ~] = size(vertexWeights);
%% Cutoff method of calculating clustering coefficients
cutoff = 0.0001;  %if weight is less than 10% of maximum edge is cut
for i = 1:l
  kE = vertexWeights{i,2};
  %normalize kE
  %kE = (kE-min(min(kE)))/(max(max(kE))-min(min(kE)))
  [n, ~] = size(kE);
  for j = 1:n
      for k = 1:n
        if kE(j,k) > cutoff
            Acut(j,k) = 1;
        else
            Acut(j,k) = 0;
        end
      end
  end
  str = 'hello'
  size(Acut)
  C = clusteringCoefficients(Acut);
  kE = C;
  k_max = 1;
  k_min = 0;
  dk = (k_max-k_min)/200;
  kDev{i} = k_min:dk:k_max;
  nu_k = hist(kE(:),kDev{i});
  Nuk{i} = nu_k/sum(nu_k);
  color = [1 0 1];
  figure
  hold on
  loglog(kDev{i},Nuk{i},'Marker','o','LineStyle','-',...
    'Color',[0 0 0],'MarkerFaceColor',color)
  ylabel('$\nu$(s)','Interpreter','latex','FontSize',30)
  xlabel('s','FontSize',30)
  title(vertexWeights{i,1})    
end




end

