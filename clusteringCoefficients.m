function [ C ] = CulsteringCoefficients(A)
%Calculates the clustering coefficients of the adjacency matrix, A, or an
%unweighted graph.  The matrix should be a graph of integers, zero or 1.

[n,~] = size(A);
for j = 1:n
  connect = 0;
  npairs = 0;
  for k= 1:n
    if j ~= k
      for m = 1:n
        if m~= j && m~=k
          if A(j,k) && A(k,m)
            npairs = npairs + 1;
            if A(j,m)
              connect = connect + 1;
            end
          end
        end
      end
    end
  end
   if npairs>0
      C(j)= connect/npairs; 
   else
      C(j) = 0;
   end


end


end
