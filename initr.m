function [r] = initr(r0, Na)
   x = r0*0.5:r0:-r0*0.5+ Na*r0;
   y =  r0*0.5:r0:-r0*0.5+ Na*r0;
   for i = 1:length(x)
       for j = 1:length(y)
         r(Na*(i-1)+j,:) = [x(i), y(j)];
       end
   end

end

