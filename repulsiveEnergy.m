function [uij] = repulsiveEnergy(rij, epsilon)
%% Calculates the attractive part of the potential energy based on a distance matrix and the defect parameter epsilon, 
%returns a matrix
[na, ~] = size(rij);
defect = ceil(na/2+sqrt(na)/2);
uij = 0*rij;
for i = 1:na
    for j= 1:na
        if rij(i,j) ~= 0
            if i ~= defect && j~=defect
                pf = 1;
            else
                pf = 1+epsilon;
            end
            uij(i,j) = 4*pf*rij(i,j)^(-12);
        else
            uij(i,j) = 0;
        end
    end
end

end

