function [finv_sparse,wij] = sparsify_force(Na,f1)
N = Na^2;
finv = zeros(N,N);
for ii = 1:N
    for jj = 1:N
        if ii~=jj
            finv(ii,jj) = abs(f1(ii,jj));
        end
    end
end
finv_sparse = sparsify_spectral(finv,1);
wij = finv_sparse./finv;
wij(isnan(wij)) = 0;