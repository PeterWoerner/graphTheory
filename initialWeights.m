%function [] = initialWeights()
Na = 6
ro = 2^(1/6)
r = initr(ro, Na)
epsilon = .01
[forceMatrix, f, fa, fr] = forcecalc(r, epsilon, Na*ro, Na*ro, ro, Na);
size(forceMatrix)
size(f)
f2 = f.*f;
fmag = (f2(:,1) + f2(:,2)).^(1/2);
size(fmag)
theta = linspace(2*pi/Na/Na,2*pi, Na*Na);
x = cos(theta); y = sin(theta)
xy = [transpose(x) transpose(y)];
weightedGraphPlot(forceMatrix(:,:, 5), [transpose(x) transpose(y)], fmag)



%end