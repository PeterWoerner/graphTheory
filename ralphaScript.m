%Script for cycling through ralphaLJ with different values for alpha.
alpha = [1, 2, 3, 4, 6, 8, 12];
n = length(alpha);
Na = 75
for i = 1:n
    lmax(i) = ralphaLJ(alpha(i), Na, true)
end
plot(alpha, lmax)
