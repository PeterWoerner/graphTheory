function [v] = initv0(r0, Na)
   x = linspace(r0, Na*r0, Na);
   y = linspace(r0, Na*r0, Na);
   for i = 1:length(x)
       for j = 1:length(y)
         v(Na*(i-1)+j,:) = [0,0];
       end
   end  
end