function [force,f, fa, fr] = forcecalc(r, epsilon, lx, ly,r0, Na)
%UNTITLED5 Calculates forces for MD simulations, assumes perfect crstal boundaries.
%   Detailed explanation goes here
[rij, dir] = distancematrix(r, r);
[fa(:,:,5), forcea(:,:,:,5)] = attractiveforce(rij, dir, epsilon);
[fr(:,:,5), forcer(:,:,:,5)] = repulsiveforce(rij,dir, epsilon);
rinit = initr(r0, Na);
shift = 0*rinit;  % shift for perfect crystal boundaries
[~, dim] = size(shift);

for i = -1:1
    for j = -1:1
        if i~=0 && j~=0
          shift(:,1) = i*lx;
          shift(:,2) = j*ly;
          [rij, dir] = distancematrix(r, rinit+shift);
          [fa(:,:, j+2+3*(i+1)), forcea(:,:,:, j+2+3*(i+1))] = attractiveforce(rij, dir, epsilon);
          [fr(:,:, j+2+3*(i+1)), forcer(:,:,:, j+2+3*(i+1))] = repulsiveforce(rij, dir, epsilon);
        end
    end
end

force = fa + fr;
f = sum(forcea,4) + sum(forcer,4);
f = sum(f,1);
q = [f(1,:,1), f(1,:,2)];
q = reshape(q,size(r));

f = q;


end

