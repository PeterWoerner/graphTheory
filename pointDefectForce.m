function [force, f] = pointDefectForce(r, atomNumber, strength, alpha)
%Calculates the force due to a pointDefect given by f = strength/r_ij^alpha
[N, ~] = size(r)
j = atomNumber
force = zeros(N)
for i = 1:N
    if i ~= j
       dr = sqrt((r(i,1)-r(j,1))^2+ (r(i,2)-r(j,2))^2);
       force(i,j) = strength/dr^alpha;
       force(j,i) = force(i,j);
    end    
end
f = sum(force);


end

