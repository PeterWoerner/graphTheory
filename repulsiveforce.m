function [fij, force] = repulsiveforce(rij, dir, epsilon)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
[na, ~, dim] = size(dir);
defect = ceil(na/2+sqrt(na)/2);
force = 0*dir;
fij = 0*rij;
for i = 1:na
    for j= 1:na
        if rij(i,j) ~= 0
            if i ~= defect && j~=defect
                pf = 1;
            else
                pf = 1+epsilon;
            end
            fij(i,j) = 48*pf*rij(i,j)^-13;
            for k = 1:dim
                force(i,j,k) = fij(i,j)*dir(i,j,k);
            end
        else
            fij(i,j) = 0;
            for k = 1:dim
                force(i,j,k) = 0;
            end
        end
    end
end


end

