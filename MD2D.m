function [r, v] = MD2D( varargin)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
tic
set(0, 'DefaultAxesFontSize', 20)
graph = true
ro = 2^(1/6)% initial distance between atoms
tf = .00020; %final time
m = 1; %mass
Nt = 100;
h = tf/Nt; %timestep
t=0:h:tf;
Na = 20; %size of atoms
r = initr(ro, Na);
v = initv0(ro, Na);
rt = zeros([size(r), 100]);
vt = rt;
if nargin >= 1
    epsilon = varargin{1}; 
    graph = false;
else
    epsilon = 0;
end
if graph == true
  figure
  scatter(r(:,1), r(:,2))
  figure
  scatter(r(:,1), r(:,2))
end

ke(1) = sum(m/2*(v(:,1).*v(:,1) + v(:,2).*v(:,2)));
[~,pe(1)] = potentialEnergy(r,epsilon, Na*ro, Na*ro);
[~,f, ~, ~] = forcecalc(r,epsilon, Na*ro, Na*ro, ro, Na);

 for i = 1:Nt

     a = f./m;
     r = r + v*h + 1/2*a*h^2;
%      trace1 = [trace1;r(t1pn,:)];
%      trace2 = [trace2;r(t2pn,:)];
%      trace3 = [trace3;r(t3pn,:)];
%      trace4 = [trace4;r(t4pn,:)];
     [~, f2, ~, ~] = forcecalc(r,epsilon, Na*ro, Na*ro, ro, Na);
     a2 = f2./m;
     v = v + h*(a+a2)/2;
     ke(i+1) = sum(m/2*(v(:,1).*v(:,1) + v(:,2).*v(:,2)));
     f = f2;
     [~,pe(i+1)] = potentialEnergy(r, epsilon, Na*ro, Na*ro);
     
%      if i > Nt-101
%          rt(:,:,i+102-Nt) = r
%          vt(:,:,i+102-Nt) = v
%      end
 end
te= ke + pe; 
size(te)
size(t)
if graph == true
hold on
scatter(r(:,1), r(:,2), 'rx')
figure 
scatter(r(:,1), r(:,2), 'rx')
end

if graph == true
   figure
   set(gca, 'fontsize', 20)
   hold on
   subplot(3,1,1)
   plot(t,ke, 'r-o')
   ylabel('KE')
   subplot(3,1,2)
   plot(t,pe, 'g-o')
   ylabel('PE')
   subplot(3,1,3)
   plot(t,te, 'b-o')
   ylabel('KE + PE')
   figure
%   plot(trace1(:,1), trace1(:,2), 'x')
end

[el, ~] = potentialEnergy(r,epsilon, Na*ro, Na*ro);

if graph == true
    figure
    hist(el)
end
save(datesave(strcat('MD2D',num2str(1000*epsilon))))
toc 
end

